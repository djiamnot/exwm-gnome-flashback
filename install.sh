#!/bin/bash

if [[ $# != 1 || !($1 != "install" || $1 != "uninstall") ]]; then
  echo "Usage: $0 install|uninstall"
  exit
fi

DESTDIR="/"
PREFIX="${DESTDIR}/usr"

case "$1" in
install)
  echo "[1/2] Installing packages..."
  sudo apt install gnome-flashback gnome-screenshot
  wait

  echo "[2/2] Installing config files..."
  sudo install -m 0644 -D files/exwm-gnome-flashback-session.desktop $PREFIX/share/xsessions/exwm-gnome-flashback-session.desktop
  sudo install -m 0644 -D files/exwm-gnome-flashback.desktop $PREFIX/share/applications/exwm-gnome-flashback.desktop
  sudo install -m 0644 -D files/exwm-gnome-flashback.session $PREFIX/share/gnome-session/sessions/exwm-gnome-flashback.session
  sudo install -m 0755 -D files/exwm-gnome-flashback-session $PREFIX/bin/exwm-gnome-flashback-session
  sudo install -m 0755 -D files/exwm-gnome-flashback $PREFIX/bin/exwm-gnome-flashback
  sudo install -m 0644 -D files/exwm-gnome-flashback.gschema.override $PREFIX/share/glib-2.0/schemas/01_exwm-gnome-flashback.gschema.override
  sudo glib-compile-schemas $PREFIX/share/glib-2.0/schemas/
  wait

  echo "Finished"
  ;;
uninstall)
  echo "[1/2] Removing config files..."
  sudo rm -f $PREFIX/bin/exwm-gnome-flashback \
    $PREFIX/bin/exwm-gnome-flashback-session \
    $PREFIX/share/gnome-session/sessions/exwm-gnome-flashback.session \
    $PREFIX/share/applications/exwm-gnome-flashback.desktop \
    $PREFIX/share/xsessions/exwm-gnome-flashback-session.desktop \
    $PREFIX/share/glib-2.0/schemas/01_exwm-gnome-flashback.gschema.override
  sudo glib-compile-schemas $PREFIX/share/glib-2.0/schemas/
  wait

  echo "[2/2] Removing packages..."
  sudo apt remove gnome-flashback gnome-screenshot
  wait

  echo "Finished"
  ;;
esac
