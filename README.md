# exwm-gnome-flashback [![LICENSE](http://img.shields.io/badge/license-MIT-blue.svg?style=flat)](http://choosealicense.com/licenses/mit/)

Allows you to use exmw in a GNOME-Flashback session. Large parts of this are based on the existing [`i3-gnome`](https://github.com/lvillani/i3-gnome) project.

I've done a full circle, the [previous version from WJCFerguson](https://github.com/WJCFerguson/exwm-gnome-flashback) stopped working for me so I went back to the source and forked the above. Just like WJCFerguson, this is mostly a `s/i3/exwm`.

This has been tested working on GNOME version **42.5**.

# Installation

For Arch users see the [AUR package](https://aur.archlinux.org/packages/i3-gnome-flashback/) for a more easy intallation.

For Ubuntu (19.10), ensure prerequisites are installed:

```
sudo apt install gnome-flashback build-essential
```
Make sure you run Emacs and your emacs is configured to to use exwm.

Then install exwm-gnome-flashback:

```
sudo make install prefix=/usr
```

(I haven't tried the default `/usr/local`)

# Notes

To understand how the files in this repo work to initialize a non-gnome WM and GNOME session, refer to this [GNOME wiki](https://wiki.gnome.org/Projects/SessionManagement/RequiredComponents) on session management.

The default session for `exwm-GNOME-Flashback` installs a set of configuration defaults for GSettings/dconf, e.g.:

  - Desktop icon handling is disabled for GNOME Flashback, due to incompatibilities with i3, and control of the root window is instead given to `gnome-control-center`, which handles setting the user-defined wallpaper, among other things.

  - Window buttons for minimize and maximize have been removed, leaving only the close button, as the former are ineffectual in i3.

Setup for these configuration defaults is handled via GSettings overrides, which are described in
further detail
[here](https://help.gnome.org/admin/system-admin-guide/stable/dconf-custom-defaults.html.en) and
[here](https://help.gnome.org/admin/system-admin-guide/stable/overrides.html.en).
